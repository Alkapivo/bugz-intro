///@function chooseFromArray(array)
///@param {Object[]} array
///@return {Object} value

	var array = argument0;
	
	var value = array[round(random(array_length_1d(array) - 1))];
	return value;
	
