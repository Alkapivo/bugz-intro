///@function logger(message, logType, parameters...)
///@description Logger
///@param {String} message
///@param {LogType} logType
///@param {Object[]} [parameters...]

	var prefix = "DEFAULT ";
	
	switch(argument[1]) {
		case LogType.INFO:
			prefix = "INFO    ";
			break;
		case LogType.WARNING:
			prefix = "WARNING ";
			break;
		case LogType.ERROR:
			prefix = "ERROR   ";
			break;
		case LogType.DEBUG:
			prefix = "DEBUG   ";
			break;
	}
	
	var message = argument[0];
	var caller = string(object_get_name(object_index)); 
	var date = string(current_year) + "-"
			+ string(string_replace(string_format(current_month, 2, 0), " ", "0")) + "-"
			+ string(string_replace(string_format(current_day, 2, 0), " ", "0")) + " "
			+ string(string_replace(string_format(current_hour, 2, 0), " ", "0")) + ":"
			+ string(string_replace(string_format(current_minute, 2, 0), " ", "0")) + ":"
			+ string(string_replace(string_format(current_second, 2, 0), " ", "0"));
	
	if (argument_count > 2) {
		for (var index = 2; index < argument_count; index++) {
			var subString = "{" + string(index - 2) + "}";
			var newString = string(argument[index]); 
			message = string_replace_all(message, subString, newString);
		}
	}
	
	var log = 
		string(date) + " " + 
		string(prefix) + "[" + 
		string(caller) + "] " + 
		string(message);
	
	show_debug_message(log);
	
	