///@function readJsonFile(path, filename)
///@description Read file to string
///@param {String} path 
///@param {String} filename 
///@return {String} data;

	var file = file_text_open_read(argument0 + argument1);
	var data = "";
	if (file == -1) {
		return "";
	}
	while (!file_text_eof(file)) {
		data += file_text_read_string(file);
		data += "\n";
		file_text_readln(file);
	}
	#region Logger
	var size = string_length(data);
	var prefix = size > 1024 ? "KB" : "B";
	size = size > 1024 ? size / 1024 : size;
	if (size > 1024) {
		prefix = "MB";
		size = size / 1024;
	}
	var decimal = 2;
	var length = string_length(string(floor(size)));
	logger("File loaded: {0}, size: {1} {2}", LogType.INFO,
		file, string_format(size, length, decimal), prefix);
	#endregion
	file_text_close(file)
	return data;