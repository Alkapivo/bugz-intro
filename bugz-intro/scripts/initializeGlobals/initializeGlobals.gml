///@function initializeGlobals()

	gml_pragma("global", "initializeGlobals()");

	#macro null noone;

	global.guiWidth = 960;
	global.guiHeight = 540;
	global.soundtrackPointer = null;
	global.isFullscreen = true;	
	
	enum LogType {
		INFO = 0,
		DEBUG = 1,
		WARNING = 2,
		ERROR = 3
	}