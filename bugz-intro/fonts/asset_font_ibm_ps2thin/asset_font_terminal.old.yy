{
    "id": "1e09510c-7d26-4e42-afaf-3968ecb6c888",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "asset_font_terminal",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Px437 IBM PS\/2thin4",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "85f1e9da-ae1c-47db-9632-fbfb3d460df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b16cdcce-bb11-4972-837b-0fbb03591647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 2,
                "x": 180,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8252ae0e-4bab-4b6d-ab66-2921c585b7a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 170,
                "y": 104
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c326131d-14b9-4c72-9414-949ef72c05f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 154,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "80a3d59f-a1c4-46c6-8007-b2d804d3087d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 138,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3160637d-7971-467b-bf58-f851d6e22aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 122,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f1f25981-95be-429d-b182-98f2cd6033ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 106,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6ea940fd-4c3b-4455-83ae-c0f45680861f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 100,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c155b2fc-5e69-4c43-88dd-c788f3d660f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 92,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "646ffc2e-8ba1-43d8-b5e6-3a45ebc4358a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 84,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "229cc395-84fc-41cf-9b43-3e48d76bab40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 184,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ef2c8174-54e4-4cb8-88d2-7de2ef1eef2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "39ea9ca2-ddde-4f52-bb1e-f630263bdd87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 48,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c34f4cc3-95a6-4261-9def-f0463d2beffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 34,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2d797f96-9ff6-4cb6-906a-93d9989b3e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 28,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f2d81a58-c93a-40e0-a79c-8dac6fadad9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 16,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7c9e43ca-60a1-44a0-8878-867c83da1b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e37a07bf-13d1-4a9b-825f-93272936ca55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 244,
                "y": 70
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c012416b-016c-473b-9e2e-86ea1d8917a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 230,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "559d0c68-e87e-4e7b-988e-7659710e2813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 216,
                "y": 70
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0d7c207a-1983-44b8-bcdb-c908d0142a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 200,
                "y": 70
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5ad38cfb-192a-4fdc-820a-f2323aaa9a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 54,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "80a7b0d9-70ed-4727-be60-72e2316c0081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 200,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a6791ea7-9a0d-44bf-a898-1db889823586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 214,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a31626c0-53c2-42c4-a821-ef5208c0da6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "77f69e1a-3ab1-4b27-8586-c961ab87d98c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 8,
                "y": 172
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "fd632138-3c25-4385-8d3b-c33b12b86cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 2,
                "y": 172
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "858c4f0b-d70b-4816-acd2-f954120f04c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 248,
                "y": 138
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "14254440-15f1-4e56-a4b0-97d2cd566d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 236,
                "y": 138
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fa632d5b-0047-4be3-be99-3a6c736bd755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 222,
                "y": 138
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2ded7641-6421-409d-a780-c6519e8b2cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 210,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "22fce2ce-87a0-402c-8b25-bcbe07181520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 196,
                "y": 138
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "78da8d66-8dd8-4c47-a1c2-8bdee4d38412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 138
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9f60b458-8e05-4e3c-9398-f99946d44359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 166,
                "y": 138
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dbc01da2-46ff-4f84-ace1-44ef35891bd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 152,
                "y": 138
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2fe5268e-32cf-4672-a793-00f0035b6895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 138,
                "y": 138
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fce5ad51-c97e-462e-a293-d8b9fc12c0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 124,
                "y": 138
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "fffeb799-b115-4605-9dfa-28759e81d738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 110,
                "y": 138
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f839106d-3469-40fe-8173-47df8db38008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 96,
                "y": 138
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "df6ffc3f-d3cb-4e69-aa55-16aa24813711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 82,
                "y": 138
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9585b8e9-2df1-49d3-a87c-e2b5aefcb3c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 68,
                "y": 138
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fa4e154e-8ae6-439d-b156-05db45f85da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 6,
                "x": 60,
                "y": 138
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b0fb2f17-c876-4430-ba97-9acd91c7ebca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 46,
                "y": 138
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7eccab94-e005-4406-b7a7-37fa879d1cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 32,
                "y": 138
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dd70040f-8416-4aca-839b-9eaa9686785c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 18,
                "y": 138
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cb662515-d943-42e8-a78b-72d15ec32493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "177399e4-c836-4ec4-ba41-2980793275de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 184,
                "y": 70
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "65be8b12-bc90-4f43-80e3-73078e2426d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 22,
                "y": 172
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a5abc9bc-4146-41b4-bcd8-f6b46fabe9f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 170,
                "y": 70
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ab7295ae-df67-4ba0-a53f-a9378a68d590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 140,
                "y": 70
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf67e300-f30c-4225-848c-1c3a707d0b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 46,
                "y": 36
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5e1469e4-4c70-4edc-b9b9-1dea2e147432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "49abec9c-e6d6-4480-b454-2d8b97e57065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 16,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "32368d2f-3078-40ff-a98a-3cffb210a773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "adfac149-e350-4235-baf7-df95d6f99b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3f74952b-6ea8-4dfa-be92-623550c21ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2526b520-df98-4015-8dee-d89b5aad92f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b71d11ae-44a8-425b-b533-b4a5486f8ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4e7efb96-63f9-4841-8a6a-452ed6599d93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "870021f1-a2d1-4e70-96cd-35f20b6c636b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5f5b3474-d918-4b32-a34c-0ddc8a631af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "82696e29-9c70-47ed-a310-fb150297a467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "44d1a6f3-1002-4ff3-aaad-861580bec466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "24c0c1a3-e200-4dd0-8c22-6af22f3f2c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "54eedd10-3ac6-47ef-86a2-930b4c28e365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b9454c87-9c5d-4c58-a787-b7147f9bb769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "12ff5318-76b3-4b3d-adee-c81cac44986a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0e3766aa-443d-44d7-8c03-cb74d33531da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d6e9750c-45b8-4020-8519-13753de98fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4ba6b645-6f63-4423-90e7-033527411e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7e8b165f-1c6e-48f2-a1e4-a991d1a4649b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0bed3d76-4277-4373-8ba1-f49fc0b6733e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 70,
                "y": 36
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ba085c1b-f405-4dbc-bcb6-e8ed511eb543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 84,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7c30a53d-ee47-4377-bb15-0088f4b1f9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 98,
                "y": 36
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2ac0f7bc-05c0-4ba1-9219-85db0e08eec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 128,
                "y": 70
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a3207524-b9fa-414e-938e-19bda86c5aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b7047b0e-e93a-4f1b-b7b5-07ae55505810",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 102,
                "y": 70
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a1ab9ecd-e8a8-4ae4-bc82-ab7663876776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 86,
                "y": 70
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "093fb123-2cbe-4452-b111-ace855ce5019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 72,
                "y": 70
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6486cfbe-022e-4497-800d-f9c29de425f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 58,
                "y": 70
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6001209f-3c10-4104-908c-71c35dc3a5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 44,
                "y": 70
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "aabe8cb3-8451-44f0-b261-60cc58a4bae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 70
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "194cebfc-8062-46c7-8528-b5495211d9da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8d2e28e6-0193-42c7-84c5-b1c0faa1bf7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "91abf79e-7c7e-41a1-9ff0-b6e63fd10235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 236,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "85b4cf89-8b80-49a5-a546-db81596121eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 222,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ae35d261-30a2-4086-94fc-d4edda1cb1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 208,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "950e2f8a-50e5-4064-b5b5-149f250897a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 192,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d1b7e0ed-1ffd-4528-b624-629346e3fd32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 178,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "84671b31-7eed-41e9-9a0d-ef349a93102e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 164,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1e4ecad2-64f9-4490-98d9-36651e4de47e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 150,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9d6c7c0b-a56b-42b4-9778-5998fc93f4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 140,
                "y": 36
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "547244f9-4f83-4b3b-8343-06ebf0cab146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 2,
                "x": 136,
                "y": 36
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1fea52cc-e31c-4cbb-8536-e0a21adc86ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 126,
                "y": 36
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "09af4782-ed89-4029-8b9a-36129a010bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 110,
                "y": 36
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "e6e75c89-554d-4d6d-9d47-7d0d2344916c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 154,
                "y": 70
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a1c7af81-820d-45a8-80ec-88643c5526ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 32,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 36,
                "y": 172
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}