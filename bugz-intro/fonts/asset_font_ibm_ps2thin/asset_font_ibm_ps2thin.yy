{
    "id": "1e09510c-7d26-4e42-afaf-3968ecb6c888",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "asset_font_ibm_ps2thin",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Px437 IBM PS\/2thin4",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ee1b65f3-82d0-4250-8303-eaa08b5c706d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1ee1856b-b697-4198-a167-dd12504894fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 2,
                "x": 292,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "15098719-0f6f-4b5f-94db-e995770ebe7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 296,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0f73d226-e6c2-4452-82df-e71f42e81be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 306,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "71f861d9-7f3e-4ca5-9d3e-07d61bd0a4a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 322,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6bbaf131-c6a7-497f-9c41-c568acdf0567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 338,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b5a01339-a33a-4c21-a28b-56d10577be99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 354,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "df27412a-a6e8-45bb-aa48-54deff2b6880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 370,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "13c5f5c4-b88c-4129-bc64-b9d3fe48d939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 376,
                "y": 70
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "56e41e56-b904-4eaa-89e3-1db9d03ad556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 384,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bf102f28-032f-4e76-9c33-e46b32c3db3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 392,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d2943744-fc8b-4398-b1a8-003977f97487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 408,
                "y": 70
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b26d1449-21fa-4717-9dac-e4a07afa8b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 424,
                "y": 70
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "12106994-5aae-4ad2-ace7-8c8376806d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 430,
                "y": 70
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "68051c7b-ee14-4c14-9e80-9f6867b90e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 444,
                "y": 70
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b861cdb9-19a4-4017-a575-9ba2b9be9538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 450,
                "y": 70
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0ca3373f-6226-453f-9aa6-d40407eecb4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 278,
                "y": 70
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bb0be8a0-d2d2-4ef1-992f-36407060dd08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 462,
                "y": 70
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a20a72e6-b1a4-4f0c-820f-c12c6e0ddaa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 264,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fecf9c30-cdea-4f91-bc61-b748e5116f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 236,
                "y": 70
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "76c5cf31-2cb2-4de8-beb5-fe6682e7b295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 40,
                "y": 70
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c95820c3-a64f-422c-8450-50fb439d3286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 56,
                "y": 70
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6e52560f-fd88-4f77-a3f5-f177e636f2b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 70,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5249ebf7-0643-4dec-a1b6-1822b8b71993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 84,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "31115983-b2e6-4279-ae07-4f112752badd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7a4e155a-9611-4b41-b9fc-5b2d385b8be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 114,
                "y": 70
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ed8ca96a-8e8a-440c-bed1-ac9a23816596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 128,
                "y": 70
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "57377067-365e-43d3-a3b5-95b07f78fea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 134,
                "y": 70
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0830ecbc-9781-4f0d-a208-b03685b1cd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 140,
                "y": 70
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c42bfb3b-0d52-4164-9a87-97edc8cba437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 152,
                "y": 70
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f8608f66-378a-4e54-aeb1-bd4e2fb459ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 166,
                "y": 70
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ee274ef2-8a1b-49e6-9af6-0589cb7731c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 178,
                "y": 70
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c1fbd795-2f80-470a-92f0-9f86489e66bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 192,
                "y": 70
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1836e54a-04ca-49e3-8d8d-fba6b257da48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 208,
                "y": 70
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f74905c1-6357-4889-b418-56d82dbb8ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 222,
                "y": 70
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "73bfe01d-2676-4dfa-8622-26b39878d60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 250,
                "y": 70
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5093ba40-6287-4541-b9c6-24fb1808f703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 472,
                "y": 70
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5d10968e-e754-4c8b-82e8-c1346381545c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 486,
                "y": 70
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "21747162-cd3c-4083-b85e-5bf366b1a97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c901776c-aad9-4554-8390-d52a60e88df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 272,
                "y": 104
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6223330b-7c90-4168-8829-51d267da788f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 286,
                "y": 104
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e551db81-506c-44d6-9237-e9e4089d4328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 6,
                "x": 300,
                "y": 104
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b7f61d20-b835-4401-a70f-64d105d32a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 308,
                "y": 104
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6ca6b726-91ed-48a9-9db2-ed9685aa3d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 322,
                "y": 104
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d322e9f6-2260-44c5-a444-911f851cd616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 336,
                "y": 104
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8580d476-c6b2-44af-8893-a9edc3d526bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 350,
                "y": 104
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "32618341-2cbb-4afd-9d23-dd330605d818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 380,
                "y": 104
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b021c7e6-c674-4ddf-95a5-20ff7be2cbdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1414bdfe-225a-47c2-8e0a-534c66ae714c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 396,
                "y": 104
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b88e5557-8d87-4e72-9511-7a8fdc486aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 410,
                "y": 104
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cbfccd84-6c81-426e-a59d-8f91ffc8b72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 424,
                "y": 104
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8231f966-201e-4a96-ae3a-47dfb32bd745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 438,
                "y": 104
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2a4f9031-5116-4c89-9752-3276e612b314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 452,
                "y": 104
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b034487f-b674-4144-93ac-8cfe027d6177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 468,
                "y": 104
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c728d649-f54d-46ed-be22-5337178890f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 482,
                "y": 104
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c548872c-dca7-493e-8fbf-0268048385b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 256,
                "y": 104
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "937d0684-a26b-4b76-b7be-de02b09c971c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 240,
                "y": 104
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b2f3bd67-82e0-4af6-b045-c4b4aae63b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 224,
                "y": 104
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3eacca2c-6a2e-40c6-8598-868c49f3b50b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 208,
                "y": 104
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "99e7475d-0fc6-4dcd-ab24-e2d61e0f30be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 16,
                "y": 104
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f628e314-5036-49c6-9e1f-7302c43e2b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 26,
                "y": 104
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "67e8a7a4-d154-4fc8-9d63-2043e440d0ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 38,
                "y": 104
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "271fd787-d7ee-4043-90c0-e65e1827a035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 48,
                "y": 104
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bdf31c78-4a12-4153-b88e-8e31e3252537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 60,
                "y": 104
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "979d0e90-e0e0-43ef-9693-cfd720950458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 78,
                "y": 104
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6df3147c-ca15-4565-b79c-c52ed2a80334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 84,
                "y": 104
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "fe6fb9d1-2adb-4279-9a7b-fec20da31c48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 98,
                "y": 104
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9d789557-1504-4a44-936f-a29b2818e76f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 112,
                "y": 104
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3cae1632-aa10-4b07-a07d-1b4e77cfdd03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 126,
                "y": 104
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "45983771-0b91-4501-8ad1-4721e1ea7dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 140,
                "y": 104
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a9af36cc-8d5c-4a3f-a39c-94e071e3fa4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 154,
                "y": 104
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1237d629-b384-4826-aa7f-c0256fb12154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 168,
                "y": 104
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "efe95f87-1c5e-4416-b032-d9a1fd1d555e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 182,
                "y": 104
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "72c9318f-e3bf-4c94-a2d2-7a9deca2c588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 196,
                "y": 104
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a9b0dfd5-2dd7-4a75-b48b-f90656de2d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 28,
                "y": 70
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b950afc0-904b-4607-ae93-f5b851396787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 14,
                "y": 70
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4d62f683-4ef3-4a57-82c3-08d7c7f55395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a949db08-5aaf-4d63-9294-1849acc48cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d7a85110-0152-4373-a078-ac4595c841e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 260,
                "y": 2
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "029e8313-729e-43c1-9675-4996ee47539e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1b7b95ee-ab13-41a0-8802-45823690148e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2e834995-5fa1-472e-b43e-abd4d52db648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "84d41090-6116-4115-97db-8811452d498f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 316,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6ce66a82-3ecd-4673-9752-9f4cdfbef3d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f72c1d04-6fda-48a2-a021-5a6387109be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5598b310-34d5-4986-a76f-99a9ccb5f0a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 358,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b167a460-6e8c-4b5b-8226-43a4401f688f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "14349ddb-5d07-4936-a742-3fe8d842001a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "da6d2d74-7632-4093-95b3-5b143dffcdb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 402,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cb394a67-7d7c-4ad1-ae8c-f56dd27d97de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 416,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8e32bfae-5521-46bc-809a-8878de529a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "87999d69-3724-4ed0-92ec-2d840d958df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 444,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "59775198-e86f-4c37-a8a5-36372658a5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 2,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "87991967-aca2-4b5f-8f6f-3adce45a65e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3e7006aa-7fa7-4c9d-aedf-07a3b3ebd469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "98f399d8-80e6-4793-9475-5d4fff089b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "3820ab14-1096-4a9b-b1ae-4b4b204f6e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 0,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "2f755de5-fc6f-4a6d-8c4d-983e0d2cf8a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 2,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "d7c7ec6c-ff43-4001-a45f-f876e7e9a545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "98e39d20-2cb3-4205-92ab-40cfb9e02f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "3e44b70d-c332-4488-bcab-2f3e28352ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "6ee638f2-2fe0-4640-a4c0-5af57b96703f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "93fe565c-b728-4ef4-8d32-9a937d6c5aca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "d280d376-c0ea-4f05-9cc5-6d23de544091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "3dff19ee-e2ad-42c7-ac06-36c1dc181bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "438f412b-c70f-40e0-a260-e154d47312ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "55ed5484-66c3-4315-82ec-2a634e6d9015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "ffebf684-a3b3-4e19-b793-d7549bdfba85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "d62d0df4-cf47-4f96-8930-8482f61e213e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "25cbb361-3556-4b44-8b59-a5fcd0fca978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "cd10e573-891a-49c7-9eae-c240e32f9877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 32,
                "offset": 6,
                "shift": 16,
                "w": 2,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "62af0d2d-fec8-47ec-95bd-9b8d55ef244d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "d66d3dd0-51a3-439d-9379-c471c9bcfa6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "6102a3aa-7dfb-45e9-ab67-af5d544e1607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "ca9c7da3-00f8-4b55-a945-b1b17f6b9073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 234,
                "y": 36
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "9ccebeee-7453-41c2-8f57-297b0ce132d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "64b1e3e8-8b6b-4fce-9500-d5ad6c71a147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 278,
                "y": 36
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "cf23372e-0bec-468a-af0b-e2e6e3bc29e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 292,
                "y": 36
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "fb2331e7-fccc-4992-91bd-981091c27610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 306,
                "y": 36
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "5943590b-4441-4eb4-b290-5c909b1196db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 322,
                "y": 36
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "8f2cf1bd-5b4b-4074-94ce-9d2821163195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 10,
                "x": 336,
                "y": 36
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "c60fb38d-9db5-485d-b8e9-62a037fdafdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 348,
                "y": 36
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "c285e2fb-693d-4085-989d-c1914d5c1319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 364,
                "y": 36
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "42e29105-c2de-4c01-bcc1-6fb082488224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 380,
                "y": 36
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "517b6fba-0e73-43d9-bd06-8f100bc17c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 396,
                "y": 36
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "d42b2a06-dd29-4ffb-a9f0-6879a27c448c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 410,
                "y": 36
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "0da5bf23-b5e1-4173-a92b-df2d02b4890f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 424,
                "y": 36
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "4625d8de-64e2-4f34-8c83-109413f73d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 438,
                "y": 36
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "9b8bdab8-7997-4951-871a-a8045a55a27e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 452,
                "y": 36
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "91b4699a-ab8f-44f1-920d-1bd54a7af6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 466,
                "y": 36
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "a4f96b40-529d-496c-8ac2-c7bd22185b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 480,
                "y": 36
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "27a30037-6ba3-4961-b9aa-ac0cdd27f77d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 264,
                "y": 36
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "a735e47d-f1a8-40ec-8ae6-f8cde1106213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 496,
                "y": 36
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "76022cdd-c3ee-4e3a-b4e4-4e2db9719909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 250,
                "y": 36
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "ad87cf08-234c-4cd0-b5de-99a179f94546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "bd93fafa-7618-4396-9181-5491830a7c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 36
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "65fd12db-ec0c-4aca-9fae-be763d44a690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "6eae59c8-ebd6-4088-8c76-05d84d5c7aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 42,
                "y": 36
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "f3526e5d-5adc-43b3-aa20-f9bdfe54e711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 54,
                "y": 36
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b41cd5db-0f21-497e-906e-dcdae7012a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 32,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "ff92ab24-ed88-4e32-91ec-2b4095255863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 78,
                "y": 36
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "e1fe41c8-fa64-4f95-af34-987892140057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 92,
                "y": 36
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "94f32696-bd54-4530-a5f1-18e24aea9103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 106,
                "y": 36
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "88d74bf3-e823-4305-87f1-9e52b9c4fadc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 120,
                "y": 36
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "2eb7c703-bf13-43df-a2bf-1245d74c707d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 134,
                "y": 36
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "1d98c741-6e71-4716-b457-0fd766c2d88d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 148,
                "y": 36
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "e2a85e39-bc83-4e23-baba-a44b5df88699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 164,
                "y": 36
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "f4e62704-3f12-46e6-92d8-6e26e4c9dca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 178,
                "y": 36
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "f824c43b-f343-40dd-baa1-0252be85ba7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 192,
                "y": 36
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "b6ac4416-e569-44e4-881e-d33859e1c880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 206,
                "y": 36
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "b626bb44-42d4-46be-bd76-0ca6c0b97b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 32,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 366,
                "y": 104
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "d778712e-c562-4733-9d4b-213ec2157ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 32,
                "offset": 6,
                "shift": 19,
                "w": 8,
                "x": 16,
                "y": 138
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}