{
    "id": "f73a5948-4ad1-4eb7-8791-e7d4a9a07c0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MenuController",
    "eventList": [
        {
            "id": "a7cea837-6c66-4cd8-b679-d2b057763ca7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f73a5948-4ad1-4eb7-8791-e7d4a9a07c0f"
        },
        {
            "id": "ac9f14a2-8076-409e-8539-4d1dc262e1bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f73a5948-4ad1-4eb7-8791-e7d4a9a07c0f"
        },
        {
            "id": "33f94ec0-b46c-44fe-bcea-de408a14fbd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f73a5948-4ad1-4eb7-8791-e7d4a9a07c0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}