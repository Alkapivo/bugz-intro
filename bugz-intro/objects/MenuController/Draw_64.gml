///@description Menu text
	
	draw_set_color(c_white);
	draw_set_alpha(1.0);
	draw_set_font(asset_font_ibm_ps2thin);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	draw_text(
		global.guiWidth / 2.0, 
		global.guiHeight / 2.0, 
		"Enter");
	