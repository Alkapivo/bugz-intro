///@description Initialize

	#region Audio
	
	global.soundtrackPointer = asset_soundtrack;
	if (!audio_is_playing(global.soundtrackPointer)) {
		audio_play_sound(global.soundtrackPointer, 100, true);
	}
	
	#endregion

	global.renderLayer = layer_get_id("render_layer");
	global.gameControllerContext = id;
	
	rawSourceCode = splitStringToArray(readStringFromFile(working_directory, "code.ts"), "\n");
	sourceCodePipeline = ds_list_create();
	ds_list_add(sourceCodePipeline, [ rawSourceCode[0], 0 ]);
	
	textVerticalMargin = 32;
	textHorizontalMargin = 18;
	textHeight = 28;
	
	timer = 0;
	timerCounter = 0;

	backgroundXPosition = 0;
	backgroundYPosition = 0;
	backgroundVerticalSpeed = 0.2;
	
	skipperRate = 48;
	skipper = skipperRate;
	
	red = 120;
    green = 30;
    blue = 200;
    

    redDirection = 1;
    greenDirection = 1;
    blueDirection = -1;
	
	
	shaderTimer = 0;
	shaderTimerPoll = [ 150, 180, 200, 250 ];
	shaderTimerTarget = chooseFromArray(shaderTimerPoll);
	shaderEnabled = false;
	shaderLifespanTimer = 0;
	shaderLifespanTimerPoll = [ 20, 40, 60, 10 ];
	shaderLifespanTimerTarget = chooseFromArray(shaderLifespanTimerPoll);
	
	#region shaderEdgeDetection
	
	#endregion