{
    "id": "79cc0384-29a8-499b-be4c-934339f1c6a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GameController",
    "eventList": [
        {
            "id": "74678d5d-9cc3-4095-bf3f-3defb3580455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "79cc0384-29a8-499b-be4c-934339f1c6a6"
        },
        {
            "id": "e95a7034-9ad7-45ad-a083-9a0c838e8076",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79cc0384-29a8-499b-be4c-934339f1c6a6"
        },
        {
            "id": "acd346f6-0542-4ca0-a7a5-1edf3259f68c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "79cc0384-29a8-499b-be4c-934339f1c6a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}