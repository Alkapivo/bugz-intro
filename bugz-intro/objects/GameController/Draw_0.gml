///@description Test


	backgroundXPosition = global.guiWidth / 2.0;
	backgroundYPosition -= backgroundVerticalSpeed;

	shaderTimer++;
	if (shaderTimer >= shaderTimerTarget) {
		shaderTimer = 0;
		shaderTimerTarget = chooseFromArray(shaderTimerPoll);
		shaderEnabled = true;
	}
	
	
	
	if (shaderEnabled) {
		shaderLifespanTimer++
		if (shaderLifespanTimer >= shaderLifespanTimerTarget) {
			shaderLifespanTimer = 0;
			shaderLifespanTimerTarget = chooseFromArray(shaderLifespanTimerPoll);
			shaderEnabled = false;	
		} else {
			shader_set(shaderEdgeDetection);	
		}
	}
	
	if (red >= 254) || (red <= 1) {
        redDirection = redDirection * -1;
    }
    if (green >= 254) || (green <= 1) {
        greenDirection = greenDirection * -1;
    }
    if (blue >= 254) || (blue <= 1) {
        blueDirection = blueDirection * -1;
    }
	
	red += 1 * redDirection
    green += 1 * greenDirection
    blue += 1 * blueDirection
	
	draw_sprite_ext(
		asset_background,
		0,
		backgroundXPosition,
		backgroundYPosition,
		1.5, 
		1.5,
		0.0,
		make_color_rgb(red, green, blue),
		1.0);

	if (shaderEnabled) {
		shader_reset();
	}


	var colors = [
		make_color_rgb(255, 255, 255),
		make_color_rgb(245, 245, 235),
		make_color_rgb(235, 215, 235),
		make_color_rgb(255, 205, 195),
		make_color_rgb(255, 255, 255),
	];
	var color = colors[round(random(array_length_1d(colors) - 1))];

	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_font(asset_font_ibm_ps2thin);
	draw_set_color(color);
	var length = ds_list_size(sourceCodePipeline)
	var textHorizontalLength = textVerticalMargin + length * textHeight;
	var textVerticalLength = global.guiHeight - textVerticalMargin;
	
	
	for (var index = 0; index < length; index += 1) {
		
		var sourceCode = sourceCodePipeline[| index];
		var line = sourceCode[0];
		var position = sourceCode[1];
		
		var maxLineLength = string_length(line);
		
		if (position >= maxLineLength) {
			position = maxLineLength;
			if (index >= length - 1) {
				var lgt = array_length_1d(rawSourceCode);

				skipper++
				if (skipper >= skipperRate) {
					if (index < lgt - 2) {
						logger("----{0} {1}", LogType.INFO, index, lgt);
						logger("{0} {1}", LogType.INFO, index, rawSourceCode[index + 1]);
						var task = [ rawSourceCode[index + 1], 0];
						ds_list_add(sourceCodePipeline, task);
					}
					skipper = 0;
				}
				
			}
		} else {
			position++;
			sourceCode[@ 1]++;
		}
		
		var text = string_copy(line, 0, position);
		var textYPosition = textVerticalMargin + index * textHeight;
		var offset = textHorizontalLength > textVerticalLength ? textHorizontalLength - textVerticalLength : 0
		
		var luck = choose(0, 0, 0, 1, 1);
		if (luck == 1) {
			var shakeX = random(2);
			var shakeY = random(2);
			draw_set_alpha(0.5);
			draw_text(
				textHorizontalMargin + shakeX,
				textYPosition - offset + shakeY,
				text);	
			draw_set_alpha(1.0);
		}
		
		draw_text(
			textHorizontalMargin,
			textYPosition - offset,
			text);
		
	}


	
