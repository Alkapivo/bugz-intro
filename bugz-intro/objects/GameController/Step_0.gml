///@description Game loop


	timerCounter++;
	if (timerCounter > room_speed * 0.1) {
		timer++;
		timerCounter = 0;
	}
	
	if (keyboard_check(vk_anykey)) {
		room_goto(MenuController);	
	}
	
	if (keyboard_check(vk_escape)) {
		game_end();	
	}
	
