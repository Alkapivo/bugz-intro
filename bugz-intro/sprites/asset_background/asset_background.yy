{
    "id": "b80da05b-7ad0-459a-8d13-9ad1347ffd6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1224,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c0307ef-5308-4263-93a1-3564b3029c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b80da05b-7ad0-459a-8d13-9ad1347ffd6e",
            "compositeImage": {
                "id": "14e5a5b3-c838-4cb6-9e08-37a007b80b88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c0307ef-5308-4263-93a1-3564b3029c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d7ee694-851c-4f25-aea6-576b758b74bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0307ef-5308-4263-93a1-3564b3029c3f",
                    "LayerId": "848e7e68-efb3-431a-a035-2fe9da6c2ee7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1225,
    "layers": [
        {
            "id": "848e7e68-efb3-431a-a035-2fe9da6c2ee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b80da05b-7ad0-459a-8d13-9ad1347ffd6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 0
}