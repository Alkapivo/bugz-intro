{
    "id": "fc312ac6-093d-4bb4-83ae-67a53a3abb36",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Syntax_Highlighted_Text",
    "IncludedResources": [
        "Scripts\\draw_text_syntax",
        "Scripts\\syntax_init",
        "Scripts\\syntax_create",
        "Scripts\\syntax_add_group",
        "Scripts\\syntax_add_keyword",
        "Scripts\\load_gml",
        "Fonts\\fn_courier_new",
        "Objects\\obj_control",
        "Rooms\\rm_test"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "ShadeSpeed",
    "classname": "",
    "copyToTargets": -1,
    "date": "2019-59-14 08:09:08",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.shadespeed.drawtextsyntax",
    "productID": "",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.0"
}